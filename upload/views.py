# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from upload.models import Files
from .forms import DocumentForm

from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings

import sys, traceback, os 

def index(request):
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        print(form)
        if form.is_valid():
            # Upload File
            upload_file(request)
            file = request.FILES['file_path']
            # Save in database
            newdoc = Files(
                user_id = 1,
                project_id = 1,
                file_path = 'media/'+file.name,
                file_name = file.name,
                file_size = file.size,
                file_type = file.content_type,
                file_ext = file.name.split('.')[1],
                file_width = 1,
                file_height = 1,
                file_tag = '',
                billboard_height = 0,
                billboard_width = 0,
                cost = 0,
                location = '',
                site_name = '',
                is_read_client = 0,
                is_read_worker = 0,
                approved = 0,
                archived = 0,
                restored = 0,
                backup = 0,
                date_upload = '2016-07-08',
                date_modified = '2016-07-08',
            )
            newdoc.save()

        # Redirect to the document list after POST
        return HttpResponseRedirect(reverse('upload.views.index'))
    else:
        form = DocumentForm() # A empty, unbound form
    
    # Load documents for the list page
    documents = Files.objects.all()

    # Render list page with the documents and the form
    return render_to_response(
        'upload/index.html', {'documents': documents, 'form': form}, RequestContext(request)
    )

def upload_file(request):
    uploaded_filename = request.FILES['file_path'].name

    # Save the uploaded file inside that folder.
    full_filename = settings.MEDIA_ROOT+"/"+uploaded_filename
    fout = open(full_filename, 'wb+')

    file_content = ContentFile(request.FILES['file_path'].read())

    # Iterate through the chunks.
    for chunk in file_content.chunks():
        fout.write(chunk)
    fout.close()
