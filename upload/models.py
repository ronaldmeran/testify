from django.db import models

# Create your models here.
class Files(models.Model):
    user_id = models.IntegerField()
    project_id = models.IntegerField()
    file_path = models.FileField()
    file_name = models.CharField(max_length=255)
    file_size = models.CharField(max_length=45)
    file_type = models.CharField(max_length=45)
    file_ext = models.CharField(max_length=10)
    file_width = models.SmallIntegerField()
    file_height = models.SmallIntegerField()
    file_tag = models.CharField(max_length=45)
    billboard_height = models.IntegerField()
    billboard_width = models.IntegerField()
    cost = models.IntegerField()
    location = models.TextField()
    site_name = models.CharField(max_length=255)
    is_read_client = models.IntegerField()
    is_read_worker = models.IntegerField()
    approved = models.IntegerField()
    archived = models.IntegerField()
    restored = models.IntegerField()
    backup = models.IntegerField()
    date_upload = models.DateTimeField()
    date_modified = models.DateTimeField()

    def __str__(self):
        return 'file_name:{0}'.format(self.file_name)

    class Meta:
        managed = False
        db_table = 'files'