from django.conf.urls import url, patterns
from django.contrib import admin
from . import views

# URL patterns
urlpatterns = [url(r'^$', views.index, name='list')]
