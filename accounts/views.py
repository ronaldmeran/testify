from django.shortcuts import render, render_to_response
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import RequestContext
from gallery.models import Project
from upload.models import Files

@login_required
def index(request):
	# Fetch values from the database
	files = Files.objects.all()

	# Render Views
	context = RequestContext(request)
	context_dict = {'title' : 'Galleria', 'favicon' : 'test', 'site_title' : 'Galleria Travel Photos', 'powered_by' : 'rmm2016', 'files' : files}
	return render_to_response('accounts/profile.html', context_dict, context)


