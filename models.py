# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desidered behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Backup(models.Model):
    uid = models.CharField(max_length=250)
    user_id = models.IntegerField()
    backup_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'backup'


class Client(models.Model):
    client_name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    company_address = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'client'


class Device(models.Model):
    user_id = models.IntegerField()
    gcm_regid = models.TextField(blank=True, null=True)
    created_by = models.IntegerField()
    date_created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'device'


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class Files(models.Model):
    user_id = models.IntegerField()
    project_id = models.IntegerField()
    file_path = models.TextField()
    file_name = models.CharField(max_length=255)
    file_size = models.CharField(max_length=45)
    file_type = models.CharField(max_length=45)
    file_ext = models.CharField(max_length=10)
    file_width = models.SmallIntegerField()
    file_height = models.SmallIntegerField()
    file_tag = models.CharField(max_length=45)
    billboard_height = models.IntegerField()
    billboard_width = models.IntegerField()
    cost = models.IntegerField()
    location = models.TextField()
    site_name = models.CharField(max_length=255)
    is_read_client = models.IntegerField()
    is_read_worker = models.IntegerField()
    approved = models.IntegerField()
    archived = models.IntegerField()
    restored = models.IntegerField()
    backup = models.IntegerField()
    date_upload = models.DateTimeField()
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'files'


class LoginAttempts(models.Model):
    ip_address = models.CharField(max_length=15)
    login = models.CharField(max_length=100)
    time = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'login_attempts'


class Notification(models.Model):
    from_user_id = models.IntegerField()
    to_user_id = models.IntegerField()
    files_id = models.IntegerField()
    type = models.CharField(max_length=20)
    message = models.CharField(max_length=100)
    status = models.IntegerField()
    is_read = models.IntegerField()
    date_created = models.DateTimeField()
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'notification'


class Project(models.Model):
    client_id = models.IntegerField()
    project_name = models.CharField(max_length=255)
    campaign_period_start = models.DateField()
    campaign_period_end = models.DateField()
    address = models.CharField(max_length=255)
    phone = models.CharField(max_length=20)
    authorized_person = models.CharField(max_length=255)
    tag = models.CharField(max_length=100)
    status = models.IntegerField()
    date_created = models.DateTimeField()
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'project'


class Role(models.Model):
    name = models.CharField(max_length=20)
    description = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'role'


class SecurityQuestions(models.Model):
    questions = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'security_questions'


class UserRemarks(models.Model):
    user_id = models.IntegerField()
    files_id = models.IntegerField()
    comment = models.CharField(max_length=255, blank=True, null=True)
    date_created = models.DateTimeField()
    date_modified = models.DateTimeField()
    tag = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'user_remarks'


class UserRoles(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING)
    role = models.ForeignKey(Role, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'user_roles'
        unique_together = (('user_id', 'role_id'),)


class UserSecurityQuestions(models.Model):
    user_id = models.IntegerField()
    security_question_id = models.IntegerField()
    answer = models.CharField(max_length=255)
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'user_security_questions'


class Users(models.Model):
    ip_address = models.CharField(max_length=15)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=255)
    salt = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=100)
    activation_code = models.CharField(max_length=40, blank=True, null=True)
    forgotten_password_code = models.CharField(max_length=40, blank=True, null=True)
    forgotten_password_time = models.IntegerField(blank=True, null=True)
    remember_code = models.CharField(max_length=40, blank=True, null=True)
    created_on = models.IntegerField()
    last_login = models.IntegerField(blank=True, null=True)
    is_login = models.IntegerField()
    active = models.IntegerField(blank=True, null=True)
    first_name = models.CharField(max_length=50, blank=True, null=True)
    middle_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    birthdate = models.DateTimeField()
    home_address = models.TextField()
    client_id = models.IntegerField()
    company = models.CharField(max_length=100, blank=True, null=True)
    company_address = models.TextField()
    province = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    position = models.CharField(max_length=100)
    phone = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'
