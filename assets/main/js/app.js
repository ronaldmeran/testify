
//--------------------//
//Fondation
//--------------------//

$(document).foundation({
    reveal : {
        animation: 'none',

    }
});



$(document).ready(function () {
  
  
    //---------------------//
    // modal box effect
    //---------------------//
  
    $('.submit-btn-to-hide').click(function(){
      $('.submit-btn-confirm').removeClass('hide');
      $('.submit-btn-wrap').addClass('hide');
      $('.comment-box, .details-wrapper select, .details-wrapper input').attr('disabled', true); 
      
    });
  
    $('.no-edit-btn').click(function(){
      $('.comment-box, .details-wrapper select, .details-wrapper input').prop("disabled", false);
      $('.submit-btn-confirm').addClass('hide');
      $('.submit-btn-wrap').removeClass('hide');
    });

    //---------------------//
    // modal button close
    //---------------------//
    $('.no-btn').click(function(){
      $('#modal-save, #modal-delete, #project-delete-img, #modal-logout').foundation('reveal', 'close');
    });
//    ('reveal', {animation: 'none'})
    

    
    //---------------------//
    //enable and disable input form
    //---------------------//
    $('#edit-btn').click(function () {
      $('#input-box-wrapper input[disabled], #input-box-wrapper select[disabled]').each(function () {
        $(this).attr('disabled', false);
      });
      $('#save-btn-modal').removeClass('hide');
      
      $('#cancel-btn').on('click',function () {
        $('#input-box-wrapper input:enabled, #input-box-wrapper select:enabled').each(function () {
          console.log('e');
          $(this).attr('disabled', true);
        });
       });
    });
    
  
  
  
});









