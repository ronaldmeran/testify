//--------------------//
//Table Area Content
//--------------------//

$(document).ready(function () {


  
  $('#s2-table').dataTable({

    //Remove to sort header part
    "bSort": true,

    //Table Height
	//"sScrollY": "401",
	//"bScrollCollapse": true,

    //Column width
    "aoColumnsDefs": [
      {
        "sWidth": "35%"
      },
      {
        "sWidth": "30%"
      },
        null

      ],

    //Pagination style next and previous
    "oLanguage": {
      "oPaginate": {
        "sNext": '<i class="fa fa-chevron-right"></i>',
        "sPrevious": '<i class="fa fa-chevron-left"></i>'
      }
    },

    //Filter Page 
    initComplete: function () {
      var column = this.api().column(2);
      var select = $('<select class="filter" ><option value="" selected>Choose Account Type</option></select>')
        .appendTo('#selectTriggerFilter')
        .on('change', function () {
          var val = $(this).val();
          column.search(val ? '^' + $(this).val() + '$' : val, true, false).draw();
        });
      column.data().unique().sort().each(function (d, j) {
        select.append('<option value="' + d + '">' + d + '</option>');
      });
    },

  });

  var s2tableimages = $('#s2-table-images').DataTable({

    //Remove to sort header part
    "bSort": false,
	
	"sPlaceHolder" : "head:before",
	
    //Column width
    "aoColumns": [
      {"sWidth": "5%"},
      { "sWidth": "25%"},
      {"sWidth": "25%"},
      {"sWidth": "18%", 'type' : 'date-range'},
      {"sWidth": "18%", 'type' : 'date-range'},
        null
      ],

    "columnDefs": [
      {
        "targets": [0],
        "searchable": false
      },
    ],

    //Pagination style next and previous
    "oLanguage": {
      "oPaginate": {
        "sNext": '<i class="fa fa-chevron-right"></i>',
        "sPrevious": '<i class="fa fa-chevron-left"></i>'
      }
    },

    //Filter Page with selection Images
    initComplete: function () {
      var selectedoption;
      var element = this;
      $('#th-selected').on('change', function () {
        selectedoption = $(this).val();
		
        if (selectedoption != '' && selectedoption != 3) 
		{			
			$('.filter').remove(); 
			$(".date-container").hide();
			$('#date-from').val('');
			$('#date-to').val('');
			s2tableimages.search( '' ).columns().search( '' ).draw();
			  var column = element.api().column(selectedoption);
			  var select = $('<select class="filter" ><option value="" selected>Select</option></select>')
				.appendTo('#select-filter-header-v2')
				.on('change', function () {
				  var val = $(this).val();
				  column.search(val ? '^' + $(this).val() + '$' : val, true, false).draw();
				});
			  column.data().unique().sort().each(function (d, j) {
				select.append('<option value="' + d + '">' + d + '</option>');
			  });
        }
		else if(selectedoption == 3)
		{
			$('.filter').remove();
			$(".date-container").show();
			s2tableimages.search( '' ).columns().search( '' ).draw();
			
			// Add event listeners to the two range filtering inputs
			$('#date-from').change( function() { s2tableimages.draw(); } );
			$('#date-to').change( function() { s2tableimages.draw(); } );
			
			var nowTemp = new Date();
			var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
			var checkin = $('#date-from').fdatepicker({
				onRender: function (date) {
					return date.valueOf() == now.valueOf() ? 'disabled' : '';
				}
			}).on('changeDate', function (ev) {
				if (ev.date.valueOf() > checkout.date.valueOf()) {
					var newDate = new Date(ev.date)
					newDate.setDate(newDate.getDate() + 1);
					checkout.update(newDate);
				}
				checkin.hide();
				$('#date-to')[0].focus();
			}).data('datepicker');
			var checkout = $('#date-to').fdatepicker({
				onRender: function (date) {
					return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
				}
			}).on('changeDate', function (ev) {
				checkout.hide();
			}).data('datepicker');
			
		}
		else 
		{
			$('.filter').remove();
			$('#date-from').val('');
			$('#date-to').val('');
			s2tableimages.search( '' ).columns().search( '' ).draw();
			$(".date-container").hide();
        }
      });
    },
  });
  
//  !Archives table
   var s2tablearchives = $('#s2-table-archives').DataTable({

    //Remove to sort header part
    "bSort": false,
    
     "bAutoWidth": false ,

    //Content display
    "lengthMenu": [
      [5, 15, 30, -1], 
      [5, 15, 30, "All"]
    ],
    
//    Column width
    "aoColumns": [
      { "sWidth": "20%" },
      { "sWidth": "30%" },
      { "sWidth": "30%" },
      null
    ],

    //Pagination style next and previous
    "oLanguage": {
      "oPaginate": {
        "sNext": '<i class="fa fa-chevron-right"></i>',
        "sPrevious": '<i class="fa fa-chevron-left"></i>'
      }
    },

        //Filter Page with selection Images
    initComplete: function () {
      var selectedoption;
      var element = this;
      $('#th-selected-archives').change(function () {
        selectedoption = $(this).val();

        if (selectedoption != '') {

          $('.filter').remove();
		  s2tablearchives.search( '' ).columns().search( '' ).draw();
          var column = element.api().column(selectedoption);
          var select = $('<select class="filter" ><option value="" selected>Select</option></select>')
            .appendTo('#select-filter-header-archives')
            .on('change', function () {
              var val = $(this).val();
              column.search(val ? '^' + $(this).val() + '$' : val, true, false).draw();
            });
          column.data().unique().sort().each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        } else {
          $('.filter').remove();
        }
      });
    },

  });
  
  //  Global Reset Table for Image
  $('#th-selected').change(function () {
    var selectedoption = $(this).val();

    if (selectedoption == '') {
      s2tableimages.search('').columns().search('').draw();
    }
  });
  
    $('#th-selected-archives').change(function () {
    var selectedoption = $(this).val();

    if (selectedoption == '') {
       s2tablearchives.search('').columns().search('').draw();
    }
	
  });

  
});