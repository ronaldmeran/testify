from django.db import models

# Create your models here.
class Project(models.Model):
    client_id = models.IntegerField()
    project_name = models.CharField(max_length=255)
    campaign_period_start = models.DateField()
    campaign_period_end = models.DateField()
    address = models.CharField(max_length=255)
    phone = models.CharField(max_length=20)
    authorized_person = models.CharField(max_length=255)
    tag = models.CharField(max_length=100)
    status = models.IntegerField()
    date_created = models.DateTimeField()
    date_modified = models.DateTimeField()

    def __str__(self):
    	return {'project_name':self.project_name, 'phone':self.phone, 'address':self.address}

    class Meta:
        managed = True
        db_table = 'project'

# class Client(models.Model):
#     client_name = models.CharField(max_length=255)
#     description = models.CharField(max_length=255)
#     company_address = models.CharField(max_length=255)

#     def __str__(self):
#         return {'client_name':self.client_name} 

#     class Meta:
#         managed = False
#         db_table = 'client'